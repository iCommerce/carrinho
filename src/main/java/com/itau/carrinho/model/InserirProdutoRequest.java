package com.itau.carrinho.model;

public class InserirProdutoRequest {
	long idProduto;

	public long getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(long idProduto) {
		this.idProduto = idProduto;
	}
	
	
}
