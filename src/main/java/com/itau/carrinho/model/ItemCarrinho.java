package com.itau.carrinho.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class ItemCarrinho {
	
	@JsonIgnore
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long IdItemCarrinho	;	
	
	@ManyToOne
	@JsonIgnore
    private Carrinho carrinho;
	
	@JsonProperty("nome")
	private String nome;
	@JsonProperty("descricao")
	private String descricao;
	@JsonProperty("preco")
	private double preco;
	@JsonProperty("imagem")
	private String imagem;
	//private String categoria;
	@JsonProperty("id")
	private long idProduto;
	
	
	public long getIdItemCarrinho() {
		return IdItemCarrinho;
	}
	public void setIdItemCarrinho(long idItemCarrinho) {
		IdItemCarrinho = idItemCarrinho;
	}
	public Carrinho getCarrinho() {
		return carrinho;
	}
	public void setCarrinho(Carrinho carrinho) {
		this.carrinho = carrinho;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public double getPreco() {
		return preco;
	}
	public void setPreco(double preco) {
		this.preco = preco;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
//	public String getCategoria() {
//		return categoria;
//	}
//	public void setCategoria(String categoria) {
//		this.categoria = categoria;
//	}
	public long getIdProduto() {
		return idProduto;
	}
	public void setIdProduto(long idProduto) {
		this.idProduto = idProduto;
	}
}

