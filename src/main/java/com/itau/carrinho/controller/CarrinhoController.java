package com.itau.carrinho.controller;


import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.itau.carrinho.model.Carrinho;

import com.itau.carrinho.model.InserirProdutoRequest;

import com.itau.carrinho.model.ItemCarrinho;


//import com.itau.produtos.*;

import com.itau.carrinho.repository.CarrinhoRepository;
import com.itau.carrinho.repository.ItemCarrinhoRepository;


@RestController
public class CarrinhoController {
	@Autowired
	CarrinhoRepository carrinhoRepository;

	@Autowired
	ItemCarrinhoRepository itemCarrinhoRepo;

	@Autowired
	RestTemplate restTemplate;


	@RequestMapping(path="/carrinho/{idSessao}", method=RequestMethod.POST)
	public ResponseEntity<?> inserirProduto(@PathVariable long idSessao,
			@RequestBody InserirProdutoRequest request){

		String url = "http://104.131.164.162:8080/produtos/" + request.getIdProduto();
		ItemCarrinho produto = restTemplate.getForObject(url, ItemCarrinho.class);
		if(produto == null) {
			return ResponseEntity.ok("null");
		}


		List<ItemCarrinho> listItemCarrinho = new ArrayList<ItemCarrinho>();
		Carrinho carrinho = new Carrinho();
		Optional<Carrinho> carrinhoBanco = carrinhoRepository.findByIdSessao(idSessao);

		if(!carrinhoBanco.isPresent()) {
			carrinho.setIdSessao(idSessao);

			Date data = new Date();
			carrinho.setDataCompra(data);
			carrinho.setQuantidadeProdutos(1);
			carrinho.setValorTotal(produto.getPreco());
			listItemCarrinho.add(produto);
			carrinho.setItemCarrinho(listItemCarrinho);
			produto.setCarrinho(carrinho);
			carrinhoRepository.save(carrinho);
			itemCarrinhoRepo.save(produto);
			return ResponseEntity.ok(carrinho);
		} else {
			produto.setCarrinho(carrinhoBanco.get());
			itemCarrinhoRepo.save(produto);

			Optional<List<ItemCarrinho>> itemCarrinhoBanco = itemCarrinhoRepo.findByCarrinho(carrinhoBanco.get());

			carrinhoBanco.get().setItemCarrinho(itemCarrinhoBanco.get());

			int quantidadeItens = carrinhoBanco.get().getQuantidadeProdutos() + 1;
			carrinhoBanco.get().setQuantidadeProdutos(quantidadeItens);

			double valorAtu = carrinhoBanco.get().getValorTotal() + produto.getPreco();
			carrinhoBanco.get().setValorTotal(valorAtu);



			carrinhoRepository.save(carrinhoBanco.get());
		}

		

		return ResponseEntity.ok(carrinhoBanco.get());
//		return ResponseEntity.ok(produto);
		//		Carrinho carrinho = new Carrinho();
		//		Optional<Carrinho> carrinhoBanco = carrinhoRepository.findByIdSessao(idSessao);
		//		List<Produtos_1> listProdutos = new ArrayList<>();

		//return ResponseEntity.ok(produto);

	}
	/*
	@RequestMapping(path="/carrinho", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Carrinho> getVariosCarrinho() {		
		return carrinhoRepository.findAll();
	}*/

	@RequestMapping(path="/carrinho/{idSessao}", method=RequestMethod.GET)
	public ResponseEntity<?> getCarrinho(@PathVariable long idSessao) {	

		Optional<Carrinho> carrinhoBanco = carrinhoRepository.findByIdSessao(idSessao);

		if(!carrinhoBanco.isPresent()) {
			return ResponseEntity.ok("null");
		}

		Optional<List<ItemCarrinho>> itemCarrinhoBanco = itemCarrinhoRepo.findByCarrinho(carrinhoBanco.get());

		if(!itemCarrinhoBanco.isPresent()) {
			return ResponseEntity.ok("null");
		}

		carrinhoBanco.get().setItemCarrinho(itemCarrinhoBanco.get());

		return ResponseEntity.ok(carrinhoBanco.get());
	}
	
	@RequestMapping(path="/carrinho/{idSessao}", method=RequestMethod.DELETE)
	public ResponseEntity<?> excluiItemCarrinho(@PathVariable long idSessao,
			@RequestBody InserirProdutoRequest request) {	
		
		Optional<Carrinho> carrinhoBanco = carrinhoRepository.findByIdSessao(idSessao);
		
		if(!carrinhoBanco.isPresent()) {
			return ResponseEntity.ok("null");
		}
		
		Optional<ItemCarrinho> deleteItemCarrinho = itemCarrinhoRepo.
				findFirstByCarrinhoAndIdProduto(carrinhoBanco.get(),request.getIdProduto());
		
		if(!deleteItemCarrinho.isPresent()) {
			Optional<List<ItemCarrinho>> itemCarrinhoBanco = itemCarrinhoRepo.findByCarrinho(carrinhoBanco.get());

			if(!itemCarrinhoBanco.isPresent()) {
				return ResponseEntity.ok("null");
			}

			carrinhoBanco.get().setItemCarrinho(itemCarrinhoBanco.get());

			return ResponseEntity.ok(carrinhoBanco.get());
		}

		itemCarrinhoRepo.delete(deleteItemCarrinho.get());
		Optional<List<ItemCarrinho>> itemCarrinhoBanco = itemCarrinhoRepo.findByCarrinho(carrinhoBanco.get());

		double valorTotal = 0;
		int quantidadeTotal = 0;
		
		
		if(!itemCarrinhoBanco.isPresent()) {
			carrinhoBanco.get().setQuantidadeProdutos(quantidadeTotal);
			carrinhoBanco.get().setValorTotal(valorTotal);
			carrinhoBanco.get().setItemCarrinho(null);
			
			carrinhoRepository.save(carrinhoBanco.get());
			return ResponseEntity.ok("null");
		}

		
		for(ItemCarrinho itemCarrinho: itemCarrinhoBanco.get()) {
			valorTotal += itemCarrinho.getPreco();
			quantidadeTotal ++;
		}
		carrinhoBanco.get().setQuantidadeProdutos(quantidadeTotal);
		carrinhoBanco.get().setValorTotal(valorTotal);
		carrinhoBanco.get().setItemCarrinho(itemCarrinhoBanco.get());
		
		return ResponseEntity.ok(carrinhoRepository.save(carrinhoBanco.get()));
		//return ResponseEntity.ok("null");

	}

}

