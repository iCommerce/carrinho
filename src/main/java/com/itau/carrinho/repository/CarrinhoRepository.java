package com.itau.carrinho.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itau.carrinho.model.Carrinho;

@Repository
public interface CarrinhoRepository extends CrudRepository<Carrinho, Long>{
	
	public Optional<Carrinho> findByIdSessao(long idSessao);

}
