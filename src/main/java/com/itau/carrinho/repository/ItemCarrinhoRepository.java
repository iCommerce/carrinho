package com.itau.carrinho.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.itau.carrinho.model.Carrinho;

import com.itau.carrinho.model.ItemCarrinho;

@Repository
public interface ItemCarrinhoRepository extends CrudRepository<ItemCarrinho, Long>{
	
	public Optional<List<ItemCarrinho>> findByCarrinho(Carrinho carrinho); 
	
	public Optional<ItemCarrinho> findFirstByCarrinhoAndIdProduto(Carrinho carrinho, long idProduto);
	
//	public void deleteByIdItemCarrinhoAndCarrinho(long IdItemCarrinho, Carrinho carrinho);

}

